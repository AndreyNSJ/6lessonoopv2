package com.company;

public class Animal {
    public double distance;
    public Animal() {
    }

    public void run(double distance){
        this.distance = distance;
        System.out.println("Animal run:" + distance + "m");
    }

    public void swim(double distance){
        this.distance = distance;
        System.out.println("Animal swam:" + distance + "m");
    }
}
